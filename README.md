# Vitalij's Instrument Group indicator for NinjaTrader 7
##### Group charts side-by-side with auto-adjusted Y-axis.

## Download
### You can get *newest version* and all *earlier releases* of this indicator in the **[download section](//bitbucket.org/ninjatrader/instrument-group/downloads)**.

![VitInstrumentGroup w/ borders](//bytebucket.org/ninjatrader/instrument-group/wiki/img/VitInstrumentGroup-1.png)
![VitInstrumentGroup w/o borders](//bytebucket.org/ninjatrader/instrument-group/wiki/img/VitInstrumentGroup-2.png)

### Download and Installation
#### [Zip-Archive for NinjaTrader 7 import](//bitbucket.org/ninjatrader/instrument-group/downloads)
#### [Install help and HowTo](//bitbucket.org/ninjatrader/instrument-group/wiki)
#### [Changelog](//bitbucket.org/ninjatrader/instrument-group/src/master/CHANGELOG.md)
#### [Commit history](//bitbucket.org/ninjatrader/instrument-group/commits)

### to create ZIP-Archive for NT7 import, run:

```sh
# zip -r -o -9 VitInstrumentGroup-latest.zip *
```

## Download and extract
Download the [Zip-Archive](https://bitbucket.org/ninjatrader/instrument-group/get/master.zip) and extract all ``*.cs`` files in to ``Documents\NinjaTrader 7\bin\Custom\Indicator`` directory.

## License

[The MIT License (MIT)](http://opensource.org/licenses/mit-license.php)

Copyright (c) 2013-2016 Vitalij <vitalij@gmx.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
