#region License and Terms

/*
The MIT License (MIT)

Copyright (c) 2013-2016 Vitalij <vitalij@gmx.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#endregion License and Terms

#region Using declarations

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using NinjaTrader.Cbi;
using NinjaTrader.Gui;
using NinjaTrader.Gui.Chart;

#endregion Using declarations

namespace NinjaTrader.Indicator {

	[Description("Group charts side-by-side with auto-adjusted Y-axis." + "\r\n" + "http://ninjatrader.bitbucket.org/")]
	public class VitInstrumentGroup : Indicator {

		// parent group form
		private InstrumentGroupForm parentForm;

		// reflection hack to subscribe to instrument-changed events
		private EventInfo instrumentSelectedEventInfo;
		private InstrumentNotifyEventHandler instrumentNotifyEventHandler;

		// charts are grouped together in one parent window by this name
		public string GroupName { get; set; }

		protected override void Initialize() {
			Overlay = true;
			AutoScale = false;
			CalculateOnBarClose = false; // if true Y-axis auto scale can't keep track

			Name = "Vitalij's Instrument Group";

			// set default group-name
			if (GroupName == null) {
				GroupName = "Group1";
			}
		}

		protected override void OnStartUp() {
			// get group form
			parentForm = InstrumentGroupForm.Get(GroupName, ChartControl.ParentForm as ChartForm);

			// allow border switching on double click
			ChartControl.ChartPanel.DoubleClick += SwitchBorder;

			// subscribe to instrument-changed event
			instrumentSelectedEventInfo = ChartControl.GetType().GetEvent("InstrumentSelected", BindingFlags.NonPublic | BindingFlags.Instance);
			instrumentNotifyEventHandler = new InstrumentNotifyEventHandler(this.OnInstrumentChanged);
			instrumentSelectedEventInfo.GetAddMethod(true).Invoke(ChartControl, new[] { instrumentNotifyEventHandler });
		}

		protected override void OnTermination() {
			// unsubscribe double click event
			ChartControl.ChartPanel.DoubleClick -= SwitchBorder;

			// unsubscribe to instrument-changed event
			instrumentSelectedEventInfo.GetRemoveMethod(true).Invoke(ChartControl, new[] { instrumentNotifyEventHandler });
		}

		private void OnInstrumentChanged(object sender, InstrumentNotifyEventArgs args) {
			// pass changed instrument to parent form to update all child forms
			parentForm.Instrument = args.Instrument;
		}

		private void SwitchBorder(object sender, EventArgs args) {
			// switch chart/window border
			// TODO: remove this? (should be only in VitWindowSettings.cs)
			if (ChartControl.ParentForm.FormBorderStyle == FormBorderStyle.None) {
				ChartControl.ParentForm.FormBorderStyle = FormBorderStyle.Sizable;
				ChartControl.ChartPanel.BorderStyle = BorderStyle.None;
			} else {
				ChartControl.ParentForm.FormBorderStyle = FormBorderStyle.None;
				ChartControl.ChartPanel.BorderStyle = BorderStyle.FixedSingle;
			}

			// adjust charts w/o border
			parentForm.Rearrange(this, EventArgs.Empty);
		}

		protected override void OnBarUpdate() {
			// only for first bars array
			if (BarsInProgress != 0) {
				return;
			}

			// adjust scale only on real-time bar changes (and last non-historical bar)
			if (
				LifeTime == LifeTimeStatus.RealTime
				||
				(CalculateOnBarClose == true && CurrentBar == BarsArray[0].Count - 2)
				||
				(CalculateOnBarClose == false && CurrentBar == BarsArray[0].Count - 1)
			) {
				parentForm.Scale();
			}
		}

		// *******************************************************************************
		// *******************************************************************************
		// *******************************************************************************

		// parent window
		private class InstrumentGroupForm : Form, IWorkspacePersistance, IInstrumentProvider {

			// static lookup registry/mapping (group-names to forms)
			static private Dictionary<string, InstrumentGroupForm> forms = new Dictionary<string, InstrumentGroupForm>();

			// find parent form for group-name
			static public InstrumentGroupForm Get(string name, ChartForm childForm) {
				InstrumentGroupForm parentForm;

				// if there is no form, create one
				if (!forms.TryGetValue(name, out parentForm)) {
					parentForm = new InstrumentGroupForm();
					parentForm.Show();

					Form old = (childForm.MdiParent == null) ? childForm : childForm.MdiParent;
					parentForm.Left = old.Left;
					parentForm.Top = old.Top;
					parentForm.Width = old.Width;
					parentForm.Height = old.Height;

					forms.Add(name, parentForm);
				}

				// remove old MDI parent and all bindings
				if (childForm.MdiParent != null && childForm.MdiParent != parentForm) {
					// NT7 compilation hack: can't cast to InstrumentGroupForm as name is overloaded after recompilation
					childForm.MdiParent.GetType().GetMethod("RemoveChild", BindingFlags.Instance | BindingFlags.Public).Invoke(childForm.MdiParent, new[] { childForm });
				}

				// convert child to MDI child form
				if (childForm.MdiParent == null) {
					parentForm.AddChild(childForm);
				}

				return parentForm;
			}

			#region IWorkspacePersistance dummies needed to seamless switch workspaces
			public NinjaTrader.Gui.WorkspaceOptions WorkspaceOptions { get; set; }
			public void Save(XmlDocument document, XmlElement element) { /*noop*/ }
			public void Restore(XmlDocument document, XmlElement element) { /*noop*/ }
			#endregion IWorkspacePersistance dummies needed to seamless switch workspaces

			public Instrument Instrument {
				get {
					return MdiChildren.OfType<IInstrumentProvider>().First().Instrument;
				}
				set {
					// update instrument for all children
					MdiChildren.OfType<IInstrumentProvider>().ToList().ForEach(form => form.Instrument = value);

					// set window caption
					this.Text = value.ToString();
				}
			}

			public InstrumentGroupForm() {
				WorkspaceOptions = new NinjaTrader.Gui.WorkspaceOptions("VitInstrumentInstrumentGroupForm-" + Guid.NewGuid().ToString("N"), this);

				// convert this window to MDI parent/container
				this.IsMdiContainer = true;
			}

			protected override void Dispose(bool disposing) {
				// remove this form from static lookup list
				forms.Where(p => p.Value == this).ToList().ForEach(p => forms.Remove(p.Key));

				ClientSizeChanged -= Rearrange;

				// clean up and close child forms
				foreach (var child in MdiChildren.OfType<ChartForm>()) {
					RemoveChild(child);
					child.Close();
				}

				base.Dispose(disposing);
			}

			public void AddChild(ChartForm child) {
				Instrument = child.Instrument;

				if (MdiChildren.Length == 0) {
					ClientSizeChanged += Rearrange;
				}

				//child.ChartControl.ChartPanel.MouseMove += BringToFront;
				child.ChartControl.ChartPanel.Click += BringToFront;
				child.ChartControl.ParentForm.ResizeEnd += Rearrange;
				child.Disposed += Rearrange;

				child.MdiParent = this;

				child.ChartControl.ParentForm.FormBorderStyle = FormBorderStyle.None;
				child.ChartControl.ChartPanel.BorderStyle = BorderStyle.FixedSingle;

				Rearrange(this, EventArgs.Empty);
			}

			public void RemoveChild(ChartForm child) {
				//child.ChartControl.ChartPanel.MouseMove -= BringToFront;
				child.ChartControl.ChartPanel.Click -= BringToFront;
				child.ChartControl.ParentForm.ResizeEnd -= Rearrange;
				child.Disposed -= Rearrange;

				child.MdiParent = null;

				if (MdiChildren.Length == 0) {
					ClientSizeChanged -= Rearrange;
				}

				child.ChartControl.ParentForm.FormBorderStyle = FormBorderStyle.Sizable;
				child.ChartControl.ChartPanel.BorderStyle = BorderStyle.None;

				Rearrange(this, EventArgs.Empty);
			}

			private void BringToFront(object sender, EventArgs args) {
				if (Form.ActiveForm != this) {
					return;
				}

				var panel = sender as Control;
				var cc = panel.Parent as ChartControl;
				var childForm = cc.ParentForm;

				if (ActiveMdiChild != childForm) {
					childForm.BringToFront();
				}
			}

			public void Rearrange(object sender, EventArgs args) {
				if (MdiChildren.Length == 0) {
					Close();
					return;
				}

				var children = MdiChildren.OrderBy(c => c.Left).ToArray();

				var width = (ClientSize.Width - 4) / children.Length;
				var height = (ClientSize.Height - 4);

				for (var i = 0; i < children.Length; i++) {
					children[i].Left = width * i;
					children[i].Top = 0;
					children[i].Width = width;
					children[i].Height = height;
				}

				Scale();
			}

			public void Scale() {
				if (MdiChildren.Length < 2) {
					return;
				}

				var children = MdiChildren.OfType<ChartForm>().OrderBy(c => c.Left);

				var max = double.MinValue;
				var min = double.MaxValue;

				// find max/min y-axis values
				foreach (var child in children) {
					var cc = child.ChartControl;

					for (int idx = cc.FirstBarPainted; idx < Math.Min(cc.LastBarPainted, cc.BarsArray[0].Count); idx++) {
						var h = cc.BarsArray[0].GetHigh(idx);
						var l = cc.BarsArray[0].GetLow(idx);

						// TODO: rewrite to if; it's faster
						max = Math.Max(max, h);
						min = Math.Min(min, l);
					}
				}

				// some arbitrary margin
				var margin = ((max - min) * 0.1);

				// for all child forms
				foreach (ChartForm c in children) {
					var cc = c.ChartControl;

					// hide all axis on charts
					cc.BarsArray[0].ScaleJustification = ScaleJustification.Overlay;

					// set y-axis to "fixed range" with given max/min values
					cc.YAxisRangeTypeLeft = Gui.Chart.YAxisRangeType.Fixed;
					cc.FixedPanelMaxLeft = max + margin;
					cc.FixedPanelMinLeft = min - margin;

					cc.YAxisRangeTypeOverlay = Gui.Chart.YAxisRangeType.Fixed;
					cc.FixedPanelMaxOverlay = max + margin;
					cc.FixedPanelMinOverlay = min - margin;

					cc.YAxisRangeTypeRight = Gui.Chart.YAxisRangeType.Fixed;
					cc.FixedPanelMaxRight = max + margin;
					cc.FixedPanelMinRight = min - margin;

					//c.BringToFront();
				}

				// show right y-axis on last chart
				children.Last().ChartControl.BarsArray[0].ScaleJustification = ScaleJustification.Right;
			}
		}

	}
}
